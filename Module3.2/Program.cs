﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Module3_2
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            int num;
            if (!int.TryParse(input, out num) || num < 0)
            {
                result = 0;
                return false;
            }
            else
            {
                result = 1;
                return true;
            }
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] mas = new int[n];
            if (n >= 1)
            {
                mas[0] = 0;
            }
            if (n >= 2)
            {
                mas[1] = 1;
            }
            if (n > 2)
            {
                for (int i = 2; i < n; i++)
                {
                    mas[i] = mas[i - 2] + mas[i - 1];
                }
            }
            return mas;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            string mun = sourceNumber.ToString();
            StringBuilder num = new StringBuilder(mun.Length);
            for (int i = mun.Length; i-- != 0;)
            {
                num.Append(mun[i]);
            }
            if (sourceNumber < 0)
            {
                num.Remove(num.Length - 1, 1);
                num.Insert(0, '-');
            }
            return int.Parse(num.ToString());
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size < 0)
            {
                size = 0;
            }
            int[] mas = new int[size];
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                mas[i] = rand.Next(-10, 10);
            }
            return mas;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for (int i = 0; i < source.Length; i++)
            {
                source[i] *= -1;
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            if (size < 0)
            {
                size = 0;
            }
            int[] mas = new int[size];
            Random rand = new Random();
            for (int i = 0; i < size; i++)
            {
                mas[i] = rand.Next(-10, 10);
            }
            return mas;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> list = new List<int>();
            for (int i = 1; i < source.Length; i++)
            {
                if (source[i] > source[i - 1])
                {
                    list.Add(source[i]);
                }
            }
            return list;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            if (size <= 0)
            {
                size = 0;
            }
            int[,] mas = new int[size, size];
            int i = 1, start = 0, side = size, x, y;
            while (i <= size * size)
            {
                x = start;
                y = start;
                for (int j = 0; j < side; j++)
                {
                    mas[x, y++] = i;
                    i++;
                }
                y--;
                for (int j = 0; j < side - 1; j++)
                {
                    mas[++x, y] = i;
                    i++;
                }
                y--;
                for (int j = 0; j < side - 1; j++)
                {
                    mas[x, y--] = i;
                    i++;
                }
                y++;
                for (int j = 0; j < side - 2; j++)
                {
                    mas[--x, y] = i;
                    i++;
                }
                start++;
                side -= 2;

            }
            return mas;
        }
    }
}
